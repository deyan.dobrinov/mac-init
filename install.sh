#!/bin/bash

RUBY_VERSION="2.6.3"

# Colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

homebrew_install() {
  echo -e "${GREEN}Installing $1...${NC}"

  if [ $(brew list | grep $1) ]; then
    echo -e "$1 is already installed.\n"
  else
    brew install $1
    echo -e "\n"
  fi
}

# Install XCode command line tools
xcode-select --install

# Create dev worskpace
echo -e "${GREEN}Creating workspace directory...${NC}"
if [ ! -d ~/dev ]; then
  mkdir ~/dev
  echo -e "Workspace directory created.\n"
else
  echo -e "Workspace directory already exists.\n"
fi

# Install homebrew
echo -e "${GREEN}Installing Homebrew...${NC}"
if ! [ -x "$(command -v brew)" ]; then
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
else
  echo -e "Homebrew already available\n"
fi

homebrew_install "git"

homebrew_install "neovim"
# TODO: Configure Nvim

homebrew_install "fzf"

homebrew_install "the_silver_searcher"

homebrew_install "fish"
# TODO: Configure Fish

homebrew_install "rbenv"
# TODO: Configure Rbenv

homebrew_install "ruby-build"
homebrew_install "htop"

# Install Ruby
echo -e "${GREEN}Installing Ruby $RUBY_VERSION...${NC}"
if [ $(rbenv versions | grep $RUBY_VERSION) ]; then
  echo "Ruby $RUBY_VERSION already installed."
else
  rbenv install $RUBY_VERSION
fi
